from __future__ import unicode_literals
import youtube_dl

links = ("https://www.youtube.com/watch?v=wbdUSJQLwds&feature=youtu.be",
         "https://www.youtube.com/watch?v=eT93WHNK5dE",
         "https://www.youtube.com/watch?v=Q4WlEdGb0FM&list=RDBLdEhlbBNkc&index=2", "https://www.youtube.com/watch?v=YE6eh_oMZv8",
         "https://www.youtube.com/watch?v=fXzIuzfAHXM",
         "https://www.youtube.com/watch?v=EMAmp6xExC4",
         "https://www.youtube.com/watch?v=wbdUSJQLwds&t=37s",
         "https://www.youtube.com/watch?v=qBzpCTo1bFA",
         "https://www.youtube.com/watch?v=O9wN88CY49I",
         "https://www.youtube.com/watch?v=xJGUgrq2Lek",
         "https://www.youtube.com/watch?v=e5ReYfX-wfs",
         "https://www.youtube.com/watch?time_continue=8&v=nPCuDh7nkE8",
         "https://www.youtube.com/watch?v=sb7nNI_9ppc",
         "https://www.youtube.com/watch?time_continue=2&v=n6R6Z7lfdTY",
         "https://www.youtube.com/watch?v=CMg-uZkUJxM",
         "https://www.youtube.com/watch?v=0k5Oshq9GVI",
         "https://www.youtube.com/watch?time_continue=1&v=TdbprqRpvDQ",
         "https://www.youtube.com/watch?time_continue=36&v=FdbwQHacmLQ",
         "https://www.youtube.com/watch?v=PD2TzBNGwEA")
ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
}

for link in links:
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([link])
