# About
This repo is created to develop python scripts to automatise the download process such as:
- Youtube videos to mp3
- Download latest Punjabi music

# Packages
```python
pip3 install youtube_dl
pip install urllib requests re
```


# Usage

On Mac
- download.py
```python
python2 download.py
```

- download_youtube.py
```python
python download_youtube.py
```

# Declarations
- download.py is tested on following websites:
  - https://mr-johal.com/
