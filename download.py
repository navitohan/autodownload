import urllib
import re
import requests


class LinkValidator:
    def __init__(self, page):
        self.page = page

    def is_valid(self):
        is_valid = False
        request = requests.get(self.page)
        if request.status_code == 200:
            print 'web site exists : ', self.page
            is_valid = True
        return is_valid


class DownloadLinks:
    def __init__(self, page, log=False):
        self.url = page
        self.log = log
        self.download_links = list()

    def get_links(self):
        print(self.url)
        self.prepare_links()
        return self.download_links

    def prepare_links(self):
        for ITEM in urllib.urlopen(self.url).readlines():
            if "https://" in ITEM:
                split = ITEM[ITEM.index("https://"):].split('"')[0]
                if "html" in split:
                    if self.log:
                        print split
                    for sub_page in urllib.urlopen(split).readlines():
                        if "touch" and "http" and ".mp3" and "/320/" in sub_page:
                            dwn = re.findall(r'(https?://\S+)', sub_page)
                            if self.log:
                                print dwn
                            if "zip" not in dwn:
                                self.download_links.append(dwn[0] + ".mp3")


class Download:
    url_list = list()
    min_size = 1000

    def __init__(self, url_list, min_size):
        self.url_list = url_list
        self.min_size = min_size

    def start(self):
        for url in self.url_list:
            print "Downloading : ", url
            r = requests.get(url, allow_redirects=True)
            if len(r.content) > self.min_size:
                open(url.split('/')[-1], 'wb').write(r.content)
            else:
                print("skipping : " + url)
        print "Items downloaded : ", len(self.url_list)

# Main
if __name__ == "__main__":
    pages = ("https://mr-johal.com/topTracks.php?cat=Single%20Track#gsc.tab=0",
             "https://mr-johal.com/topTracks.php?cat=Punjabi#gsc.tab=0")
    for page in pages:
        if LinkValidator(page).is_valid():
            url_list = DownloadLinks(page, True)
            run = Download(url_list.get_links(), 1000)
            run.start()
        else:
            print ("Page is not valid")

